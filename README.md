# Mascot

[![pipeline status](https://gitlab.com/mathezirkel/mascot/badges/master/pipeline.svg)](https://gitlab.com/mathezirkel/mascot/-/commits/master) [![coverage report](https://gitlab.com/mathezirkel/mascot/badges/master/coverage.svg)](https://gitlab.com/mathezirkel/mascot/-/commits/master)

Mascot stands for _**M**atheschülerzirkel **A**ug**S**burg **CO**mmand **T**ool_.

Mascot is the CLI tool for accessing the Matheschüerzirke Augsburg database.

## Usage

TBW

## Build and Run Tests

Requirements:
* A current version of [SBT](https://www.scala-sbt.org/download.html) installed
* JDK (at least version 11) installed, e.g. from [AdoptOpenJDK](https://adoptopenjdk.net/)
* A running Postgresql server, e.g. by running the docker compose file [docker/docker-compose.yml]()

For setting up the DB for integration tests, copy `docker/.env.example` to `/docker/.env`
and modify it appropriately. Then launch Postgresql by executing

```shell
$ cd docker
$ docker-compose up -d
```

In order to configure the integration tests to use the running Postgresql server, copy
`src/it/resources/local.conf.example` to `src/it/resources/local.conf` and modify the file
appropriately. Once the database connection is set up correctly, you can run the tests by executing

```shell
sbt clean test it:test
```

## Licence

Mascot is published under GPL 3, see [LICENCE](LICENSE).
