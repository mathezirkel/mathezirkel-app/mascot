import java.net.URL

import Dependencies._

lazy val mascot = project
  .in(file("."))
  .settings(
    inThisBuild(
      List(
        organization := "de.musmehl.mathezirkel",
        organizationName := "Matheschülerzirkel Augsburg",
        organizationHomepage := Some(new URL("https://www.math.uni-augsburg.de/schueler/mathezirkel/")),
        licenses += ("GPL-3.0", new URL("https://www.gnu.org/licenses/gpl-3.0.en.html")),
        scalaVersion := "2.13.4",
        // Since we use sbt-gitlab for dependency resolution via Gitlab, we need to use Ivy
        useCoursier := false,
        // Add Mathezirkel Gitlab package repository to resolve tuba dependency
        resolvers += "Gitlab Mathezirkel Repository" at "https://gitlab.com/api/v4/projects/14081723/packages/maven",
        scalacOptions ++= Seq(
          "-deprecation",                              // Emit warning and location for usages of deprecated APIs.
          "-explaintypes",                             // Explain type errors in more detail.
          "-feature",                                  // Emit warning and location for usages of features that should be imported explicitly.
          "-language:existentials",                    // Existential types (besides wildcard types) can be written and inferred
          "-language:higherKinds",                     // Allow higher-kinded types
          "-unchecked",                                // Enable additional warnings where generated code depends on assumptions.
          "-Xcheckinit",                               // Wrap field accessors to throw an exception on uninitialized access.
          "-Xfatal-warnings",                          // Fail the compilation if there are any warnings.
          "-Xlint:adapted-args",                       // Warn if an argument list is modified to match the receiver.
          "-Xlint:constant",                           // Evaluation of a constant arithmetic expression results in an error.
          "-Xlint:doc-detached",                       // A Scaladoc comment appears to be detached from its element.
          "-Xlint:inaccessible",                       // Warn about inaccessible types in method signatures.
          "-Xlint:infer-any",                          // Warn when a type argument is inferred to be `Any`.
          "-Xlint:missing-interpolator",               // A string literal appears to be missing an interpolator id.
          "-Xlint:nullary-unit",                       // Warn when nullary methods return Unit.
          "-Xlint:package-object-classes",             // Class or object defined in package object.
          "-Xlint:poly-implicit-overload",             // Parameterized overloaded implicit methods are not visible as view bounds.
          "-Xlint:private-shadow",                     // A private field (or class parameter) shadows a superclass field.
          "-Xlint:stars-align",                        // Pattern sequence wildcard must align with sequence component.
          "-Xlint:type-parameter-shadow",              // A local type parameter shadows a type already in scope.
          "-Ywarn-dead-code",                          // Warn when dead code is identified.
          "-Ywarn-extra-implicit",                     // Warn when more than one implicit parameter section is defined.
          "-Ywarn-unused:implicits",                   // Warn if an implicit parameter is unused.
          "-Ywarn-unused:imports",                     // Warn if an import selector is not referenced.
          "-Ywarn-unused:locals",                      // Warn if a local definition is unused.
          "-Ywarn-unused:params",                      // Warn if a value parameter is unused.
          "-Ywarn-unused:patvars",                     // Warn if a variable bound in a pattern is unused.
          "-Ywarn-unused:privates",                    // Warn if a private member is unused.
          "-Ycache-plugin-class-loader:last-modified", // Enables caching of classloaders for compiler plugins
          "-Ycache-macro-class-loader:last-modified"   // and macro definitions. This can lead to performance improvements.
        )
      )
    )
  )
  .settings(
    name := "Mascot",
    startYear := Some(2020),
    libraryDependencies ++= List(
      `circe-core`,
      `circe-generic`,
      `circe-parser`,
      `circe-yaml`,
      scallop,
      tuba,
      scalatest                   % Test,
      scalacheck                  % Test,
      `scalatest-plus-scalacheck` % Test
    )
  )
  // Integration tests
  .configs(IntegrationTest)
  .settings(
    Defaults.itSettings,
    IntegrationTest / parallelExecution := false,
    libraryDependencies ++= List(
      scalatest   % IntegrationTest,
      `slf4j-nop` % IntegrationTest
    )
  )
  // Build Info
  .enablePlugins(BuildInfoPlugin)
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "de.musmehl.mathezirkel.mascot",
    buildInfoUsePackageAsPath := true
  )
  // Build a release
  .enablePlugins(JavaAppPackaging)
  .settings(
    maintainer := "sven@musmehl.de" // Find permanent email address
  )
